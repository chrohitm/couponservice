package com.couponApp.otp.model;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class OTPModel {
    private String mobile_no;
    private String otp;
    private String insert_date;

    private static final Map<String ,OTPModel > otpMap = null;

    public OTPModel() {
    }

    public OTPModel(String mobile_no, String otp, String insert_date) {
        this.mobile_no = mobile_no;
        this.otp = otp;
        this.insert_date = insert_date;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(String insert_date) {
        this.insert_date = insert_date;
    }

    public static final Map<String ,OTPModel > getOtpMap() {
        if(otpMap==null){
            return new HashMap<>();
        }
        return otpMap;
    }
    @Override
    public String toString() {
        return "OTPModel{" +
                "mobile_no='" + mobile_no + '\'' +
                ", otp='" + otp + '\'' +
                ", insert_date='" + insert_date + '\'' +
                '}';
    }
}
