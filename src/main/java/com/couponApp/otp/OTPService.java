package com.couponApp.otp;

import com.couponApp.otp.model.OTPModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class OTPService {
    public static Logger log = LoggerFactory.getLogger(OTPService.class);

    @Autowired
    public OTPModel otpModel;

    public OTPModel createOTP(String mobileNumber) {
        otpModel.setMobile_no(mobileNumber);
       // otpModel.setOtp(new SimpleDateFormat("SSSS").format(new Date().getTime()));
        otpModel.setOtp("1111");
        otpModel.setInsert_date(new SimpleDateFormat("HHmmss").format(new Date()));
        OTPModel.getOtpMap().put(mobileNumber, otpModel);
        return otpModel;
    }

    public boolean validateOtp(String mobileNum, String otpGiven){
        log.info("Verifying otp for mobile number : ",mobileNum);
        OTPModel otpModel = OTPModel.getOtpMap().get(mobileNum);
        if(otpModel!=null && otpModel.getOtp()!=null && otpGiven.length()==4){
            if(otpGiven.equalsIgnoreCase(otpModel.getOtp()) && verifyOtpTime(otpModel.getInsert_date())){
                log.info("Otp is verified and removed");
                OTPModel.getOtpMap().remove(mobileNum);
                return true;
            }
        }
        return false;
    }

    private boolean verifyOtpTime(String otpCreatedTime){
        if(otpCreatedTime!=null && otpCreatedTime.length()>0){
            return Integer.valueOf(otpCreatedTime)-Integer.valueOf(new SimpleDateFormat("HHmmss")
                    .format(new Date()))<=000200;
        }
        return false;
    }
}
