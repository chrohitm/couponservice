package com.couponApp.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@AllArgsConstructor
@Data
@Entity(name = "user")
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long USER_ID;
    @Column()
    private String MOBILE_NO;
    @Column()
    private String INSERT_DATE;
    @Column()
    private String LAST_MODIFIED;
}
