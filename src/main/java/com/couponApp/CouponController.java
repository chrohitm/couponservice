package com.couponApp;

import com.couponApp.model.Coupon;
import com.couponApp.service.CouponService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/couponApi")
public class CouponController {
    @Autowired
    CouponService couponService;

    @PostMapping(value = "/add/coupon")
    public Coupon couponData(@RequestBody Coupon cpnModel) {
        return couponService.addCoupon(cpnModel);
    }

    @GetMapping(value = "/find/all")
    public List<Coupon> getAllCoupon() {
        return couponService.getAllCoupon();
    }

    @GetMapping(value = "/get/{id}")
    public Coupon getCouponById(@PathVariable Long id) {
        return couponService.getCouponById(id);
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteCoupon(@PathVariable Long id) {
        couponService.deleteCouponByID(id);
    }
}
