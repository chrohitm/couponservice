package com.couponApp;

import com.couponApp.model.User;
import com.couponApp.repo.UserDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
@Slf4j
@RestController
public class UserController {
    @Autowired
    UserDao userDao;


    @GetMapping(value = "/get/allusers")
    public List<User> getAllUsers() {
        log.info("finding user.");
        List<User> userList = userDao.findAll();
        log.info("user data list.{}", userList);
        return userList;
    }

    @PostMapping(value = "/save/user")
    public ResponseEntity saveUsers(@RequestBody User user) {
        log.info("finding user.");
        userDao.save(user);
        log.info("user data list.{}", user);
        return new ResponseEntity(HttpStatus.OK);
    }
}
