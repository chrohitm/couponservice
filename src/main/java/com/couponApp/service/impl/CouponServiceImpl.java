package com.couponApp.service.impl;

import com.couponApp.model.Coupon;
import com.couponApp.repo.CouponRepo;
import com.couponApp.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
@Service
public class CouponServiceImpl implements CouponService {

    @Autowired
    CouponRepo couponRepo;

    @Transactional
    @Override
    public Coupon addCoupon(Coupon coupon) {
        return couponRepo.saveAndFlush(coupon);
    }

    @Override
    public List<Coupon> getAllCoupon() {
        return couponRepo.findAll();
    }

    @Override
    public Coupon getCouponById(Long id) {
        return couponRepo.findById(id).get();
    }

    @Override
    public Coupon findBeforeProvidedDate() {
        return null;
    }

    @Override
    public void deleteCouponByID(Long id) {
        couponRepo.deleteById(id);
    }

}
