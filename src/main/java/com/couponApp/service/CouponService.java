package com.couponApp.service;

import com.couponApp.model.Coupon;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface CouponService {

    Coupon addCoupon(Coupon coupon);
    List<Coupon> getAllCoupon();
    Coupon getCouponById(Long id);
    Coupon findBeforeProvidedDate();
    void deleteCouponByID(Long id);



}
