package com.couponApp;

import com.couponApp.otp.OTPService;
import com.couponApp.otp.model.OTPModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/otp")
public class OTPController {
    public Logger log = LoggerFactory.getLogger(OTPController.class);

    @Autowired
    OTPService otpService;

    @GetMapping(value = "/getMobile")
    public String getMobileNumber() {
        return "+919568555991";
    }

    @PostMapping(value = "/get/otp")
    public OTPModel getOtp(@RequestBody String mobileNumber) {
        log.info("Creating OTP");
        return otpService.createOTP(mobileNumber);
    }

    @PostMapping(value = "/verify/otp",consumes = "application/json")
    public ResponseEntity verifyOtp(@RequestBody OTPModel otpmodel ){
        boolean auth = otpService.validateOtp(otpmodel.getMobile_no(), otpmodel.getOtp());
        if(auth){
            return new ResponseEntity("OTP is verified", HttpStatus.OK);
        }
        return new ResponseEntity("OTP is not verified", HttpStatus.NOT_FOUND);
    }
}
